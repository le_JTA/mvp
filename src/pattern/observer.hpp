#ifndef __OBSERVER_HPP__
#define __OBSERVER_HPP__

namespace pattern {

class Subject;

class Observer {
public:
	virtual ~Observer() = default;
	virtual void update(Subject* subject) = 0;
};

} // namespace pattern

#endif // __OBSERVER_HPP__
