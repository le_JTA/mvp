#ifndef __SUBJECT_HPP__
#define __SUBJECT_HPP__

#include <vector>

namespace pattern {

class Observer;

class Subject {
public:
	virtual ~Subject(){}
	void attach(Observer*);
	void detach(Observer*);
	void notify();

private:
	std::vector<class Observer*> observers_;
};

} // namespace pattern

#endif // __SUBJECT_HPP__
