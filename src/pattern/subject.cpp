#include "subject.hpp"
#include "observer.hpp"

namespace pattern {

void Subject::attach(Observer* obs)
{
	observers_.push_back(obs);
}

void Subject::detach(Observer* obs)
{
	for (auto it = observers_.begin(); it != observers_.end(); ++it) {
		if ( *it == obs) {
			observers_.erase(it);
			break;
		}
	}
}

void Subject::notify()
{
	for (Observer* observer : observers_) {
		observer->update(this);
   }
}

} // namespace pattern
