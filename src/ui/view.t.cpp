#define BOOST_TEST_MODULE "view test module"
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "view.hpp"

BOOST_AUTO_TEST_CASE(view_case)
{
	int i = 2;
	BOOST_TEST(i);
	BOOST_TEST(i == 2);
}
