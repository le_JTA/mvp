#include "view.hpp"

namespace ui {

View::View() :
	vbox(Gtk::ORIENTATION_VERTICAL, 5),
	button_box(Gtk::ORIENTATION_HORIZONTAL),
	button_start("Start work"),
	button_stop("Stop work"),
	button_quit("_Quit", /* mnemonic= */ true),
	progress_bar(),
	scrolled_window(),
	text_view()
{
	set_title("Model view presenter example");
	set_border_width(5);
	set_default_size(300, 300);

	add(vbox);
	// Add the ProgressBar.
	vbox.pack_start(progress_bar, Gtk::PACK_SHRINK);

	progress_bar.set_text("Fraction done");
	progress_bar.set_show_text();

	// Add the TextView, inside a ScrolledWindow.
	scrolled_window.add(text_view);

	// Only show the scrollbars when they are necessary.
	scrolled_window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	vbox.pack_start(scrolled_window);

	text_view.set_editable(false);
	button_stop.set_sensitive(false);

	// Add the buttons to the ButtonBox.
	vbox.pack_start(button_box, Gtk::PACK_SHRINK);

	button_box.pack_start(button_start, Gtk::PACK_SHRINK);
	button_box.pack_start(button_stop, Gtk::PACK_SHRINK);
	button_box.pack_start(button_quit, Gtk::PACK_SHRINK);
	button_box.set_border_width(5);
	button_box.set_spacing(5);
	button_box.set_layout(Gtk::BUTTONBOX_END);

	// Create a text buffer mark for use in update_widgets().
	auto buffer = text_view.get_buffer();
	buffer->create_mark("last_line", buffer->end(), /* left_gravity= */ true);

	show_all_children();
}

} // namespace ui
