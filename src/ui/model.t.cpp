#define BOOST_TEST_MODULE "model test module"
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "model.hpp"

BOOST_AUTO_TEST_CASE(model_case)
{
	int i = 2;
	BOOST_TEST(i);
	BOOST_TEST(i == 2);
}
