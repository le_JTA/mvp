#ifndef __VIEW_HPP__
#define __VIEW_HPP__

#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/box.h>
#include <gtkmm/textview.h>
#include <gtkmm/progressbar.h>

namespace ui {

class View : public Gtk::Window {
public:
	View();
	virtual ~View() = default;

	Gtk::Box					vbox;
	Gtk::ButtonBox 		button_box;
	Gtk::Button 			button_start;
	Gtk::Button 			button_stop;
	Gtk::Button 			button_quit;
	Gtk::ProgressBar	 	progress_bar;
	Gtk::ScrolledWindow	scrolled_window;
	Gtk::TextView 	      text_view;
};

} // namespace ui

#endif // __VIEW_HPP__
