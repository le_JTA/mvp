#ifndef __PRESENTER_HPP__
#define __PRESENTER_HPP__

#include "pattern/observer.hpp"

#include <memory>

namespace Glib { class Dispatcher; }
namespace pattern { class Subject; }
namespace worker { class Simple_worker; }
namespace std { class thread; }

namespace ui {

class Model;
class View;

class Presenter : public pattern::Observer {
public:
	Presenter(View&, Model&);
	virtual ~Presenter() override;

	// Observer update function
	void update(pattern::Subject*) override;

	// Signal handlers
	void on_start_work_button_clicked();
	void on_stop_work_button_clicked();
	void on_quit_button_clicked();

	// Dispatcher handler
	void on_notification_from_worker_thread();

private:
	// View and Model
	Model& model_;
	View& view_;

	std::unique_ptr<worker::Simple_worker> worker_;
	std::unique_ptr<std::thread>				worker_thread_;
	std::unique_ptr<Glib::Dispatcher>		dispatcher_;
};

} // namespace ui
#endif // __PRESENTER_HPP__
