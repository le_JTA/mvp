#define BOOST_TEST_MODULE "presenter test module"
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "presenter.hpp"

BOOST_AUTO_TEST_CASE(presenter_case)
{
	int i = 2;
	BOOST_TEST(i);
	BOOST_TEST(i == 2);
}
