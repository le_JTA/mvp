#include "presenter.hpp"
#include "model.hpp"
#include "view.hpp"
#include "worker/simple_worker.hpp"

#include <iostream>
#include <string>
#include <random>
#include <thread>
#include <glibmm/dispatcher.h>

namespace ui {

Presenter::Presenter(View& view, Model& model) :
	model_(model),
	view_(view),
	worker_(std::make_unique<worker::Simple_worker>()),
	worker_thread_(nullptr),
	dispatcher_(std::make_unique<Glib::Dispatcher>())
{
	// Attach Presenter(observer) to the worker(subject) (Observer pattern)
	worker_->attach(this);

	// Connect the signal handlers to the buttons.
	view_.button_start.signal_clicked().connect(
		sigc::mem_fun(*this, &Presenter::on_start_work_button_clicked)
	);
	view_.button_stop.signal_clicked().connect(
		sigc::mem_fun(*this, &Presenter::on_stop_work_button_clicked)
	);
	view_.button_quit.signal_clicked().connect(
		sigc::mem_fun(*this, &Presenter::on_quit_button_clicked)
	);

	// Connect the handler to the dispatcher.
	dispatcher_->connect(
		sigc::mem_fun(*this, &Presenter::on_notification_from_worker_thread));
}

Presenter::~Presenter()
{
	if (worker_thread_ && worker_thread_->joinable()) {
		worker_->stop();
		worker_thread_->join();
	}
}

void Presenter::update(pattern::Subject* subject)
{
	dispatcher_->emit();
}

void Presenter::on_notification_from_worker_thread()
{
	if (worker_thread_ && worker_->has_stopped())
	{
		// Work is done.
		if (worker_thread_->joinable()) {
			worker_thread_->join();
		}
		worker_thread_.reset(nullptr);
		view_.button_start.set_sensitive(true);
      view_.button_stop.set_sensitive(false);
	}

	// get progress status
   double fraction_done = worker_->get_fraction();

   // Set message string
   std::ostringstream ostr;
   ostr << (fraction_done * 100.0) << "% done\n";
   Glib::ustring message = ostr.str();


   if (message != view_.text_view.get_buffer()->get_text()) {
		view_.progress_bar.set_fraction(fraction_done);

		auto buffer = view_.text_view.get_buffer();
		buffer->set_text(message);

		// Scroll the last inserted line into view. That's somewhat complicated.
		Gtk::TextIter iter = buffer->end();
		iter.set_line_offset(0); // Beginning of last line
		auto mark = buffer->get_mark("last_line");
		buffer->move_mark(mark, iter);
		view_.text_view.scroll_to(mark);
   }
}

void Presenter::on_start_work_button_clicked()
{
	if (worker_thread_) {
		std::cerr << "Can't start a worker thread while"
					 << " another one is running." << std::endl;
	}
	else {
		worker_thread_ = std::make_unique<std::thread>(
					[this](){ worker_->run(); }
		);
      view_.button_start.set_sensitive(false);
      view_.button_stop.set_sensitive(true);
	}
}

void Presenter::on_stop_work_button_clicked()
{
	if (!worker_thread_) {
		std::cout << "Can't stop a worker thread. None is running." << std::endl;
	}
	else {
		worker_->stop();
		view_.button_start.set_sensitive(true);
      view_.button_stop.set_sensitive(false);
	}
}

void Presenter::on_quit_button_clicked()
{
	if (worker_thread_ && worker_thread_->joinable()) {
		worker_->stop();
		worker_thread_->join();
	}
	view_.hide();
}

} // namespace ui
