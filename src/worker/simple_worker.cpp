#include "simple_worker.hpp"
#include "pattern/subject.hpp"

#include <thread>
#include <chrono>
#include <iostream>

namespace worker {

Simple_worker::Simple_worker() :
	mutex_(),
	shall_stop_(false),
	has_stopped_(false),
	fraction_done_(0.0)
{
}

void Simple_worker::run()
{
	std::unique_lock<std::mutex> lk(mutex_);
   has_stopped_ = false;
   fraction_done_ = 0.0;
   lk.unlock();

   while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

		lk.lock();

		fraction_done_ += 0.01;
		if (fraction_done_ >= 1.0) {
			lk.unlock();
			break;
		}
		if (shall_stop_) {
			lk.unlock();
			break;
		}

		lk.unlock();
		notify();
   }

   lk.lock();
   shall_stop_ = false;
   has_stopped_ = true;
   lk.unlock();

   notify();
}

void Simple_worker::stop()
{
	std::lock_guard<std::mutex> lk(mutex_);
	shall_stop_ = true;
}

double Simple_worker::get_fraction() const
{
	std::lock_guard<std::mutex> lk(mutex_);
	return fraction_done_;
}

bool Simple_worker::has_stopped() const
{
	std::lock_guard<std::mutex> lk(mutex_);
	return has_stopped_;
}

} // namespace worker
