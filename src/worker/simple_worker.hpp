#ifndef __SIMPLE_WORKER_HPP__
#define __SIMPLE_WORKER_HPP__

#include "pattern/subject.hpp"
#include <mutex>

namespace worker {

class Simple_worker : public pattern::Subject {
public:
	Simple_worker();
	~Simple_worker() = default;

	void run();
	void stop();
	double get_fraction() const;
	bool has_stopped() const;

private:
	mutable std::mutex mutex_;
	bool shall_stop_;
	bool has_stopped_;
	double fraction_done_;
};

} // namespace worker

#endif // __SIMPLE_WORKER_HPP__
