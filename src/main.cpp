#include "ui/view.hpp"
#include "ui/presenter.hpp"
#include "ui/model.hpp"

int main(int argc, char* argv[])
{
	auto app = Gtk::Application::create(argc, argv, "org.gtkmm.mvp");

	ui::View view;
	ui::Model model;
	ui::Presenter presenter{view, model};

	return app->run(view);
}
